import React, { Component } from 'react'
import './Calculator.css'
export class Calculator extends Component {
    state = {
        value: 0,
        displayValue: '0',
        operator: null,
        waitingForOperand: false,
        func_keys_digit : ['AC','+/-','%','7','8','9','4','5','6','1','2','3','0','.'],
        operator_keys:['/','X','-','+','=']

    }
    inputDigit(digit) {
        const { displayValue, waitingForOperand } = this.state
        if(digit === '.'){
            if (waitingForOperand) {
                this.setState({
                    displayValue: '.',
                    waitingForOperand: false
                })
            }
            else if (displayValue.indexOf('.') === -1) {
                this.setState({
                    displayValue: displayValue + '.'
                })
            }
        }
        else if(digit === 'AC'){
            this.setState({
                value: 0,
                displayValue: '0',
                operator: null,
                waitingForOperand: false
            })
        }
        else if(digit === '+/-'){
            this.setState({
                displayValue: displayValue.charAt(0) === '-' ? displayValue.substr(1) : '-' + displayValue
            })
        }
        else if(digit === '%'){
            const value = parseFloat(displayValue)
            this.setState({
                displayValue: String(value / 100)
            })
        }
        else{
            if (waitingForOperand) {
                this.setState({
                    displayValue: String(digit),
                    waitingForOperand: false
                })
            }
            else {
                this.setState({
                    displayValue: displayValue === '0' ? String(digit) : displayValue + digit
                })
            }
        }
    }
    calcOperation(nextOperator) {
        if(nextOperator ==='X')
            nextOperator = '*'
        const { displayValue, operator, value } = this.state
        const nextValue = parseFloat(displayValue)
        const operations = {
            '/': (prevValue, nextValue) => prevValue / nextValue,
            '*': (prevValue, nextValue) => prevValue * nextValue,
            '+': (prevValue, nextValue) => prevValue + nextValue,
            '-': (prevValue, nextValue) => prevValue - nextValue,
            '=': (prevValue, nextValue) => nextValue,
        }

        if (value === 0) {
            this.setState({
                value: nextValue
            })

        }
        else if (operator) {
            const currentValue = value || 0
            const computedValue = operations[operator](currentValue, nextValue)
            this.setState({
                waitingForOperand: false,
                operator: null,
                displayValue: computedValue,
                value:0
            })

        }
        this.setState({
            waitingForOperand: true,
            operator: nextOperator
        })


    }
    render() {
        return (
            <div className="calculator">
                <div className="calc_display">
                    <div className="value">
                        {this.state.displayValue}
                    </div>
                </div>

                <div className="calc_keypad">
                    <div className="func_keys_digit">
                        {this.state.func_keys_digit.map(key=>{
                            return   <button key={key} className="calc_keys" onClick={() => this.inputDigit(key)}>{key}</button>
                        })}
                    </div>
                    <div className="operator_keys">
                        {this.state.operator_keys.map(key=>{
                            return   <button key={key} className="calc_keys " onClick={() => this.calcOperation(key)}>{key}</button>
                        })}
                    </div>

                </div>
            </div>
        )
    }
}

export default Calculator
